#ifndef UTILITY_H__
#define UTILITY_H__

#include <mpi.h>
#include <stdio.h>

#define STR(x) #x
#define SLINE__ STR(__LINE__)

#define gcc_unlikely(expr) __builtin_expect(!!(expr), 0)
#define gcc_likely(expr)   __builtin_expect(!!(expr), 1)

#define MPI_GUARDED_CALL(call)                                                                          \
    do {                                                                                                \
        int errcode = 0;                                                                                \
        if(gcc_unlikely(MPI_SUCCESS != (errcode = call))) {                                             \
            int errstrlen = 0;                                                                          \
            char errstring[MPI_MAX_ERROR_STRING + 1] = {};                                              \
            MPI_Error_string(errcode, errstring, &errstrlen);                                           \
            fprintf(stderr, "`" #call "`:" __FILE__ ":" SLINE__ ":%d:%s\n", errcode, errstring);        \
            fflush(stderr);                                                                             \
            MPI_Abort(MPI_COMM_WORLD, errcode);                                                         \
        }                                                                                               \
    } while(0)

#define MPI_MAIN_TAG(errhandler, code...)                                                               \
    {                                                                                                   \
        MPI_Init(&argc, &argv);                                                                         \
        MPI_Comm_set_errhandler(MPI_COMM_WORLD, errhandler);                                            \
                                                                                                        \
        int mpi_rank = 0; MPI_GUARDED_CALL(MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank));                   \
        int mpi_size = 0; MPI_GUARDED_CALL(MPI_Comm_size(MPI_COMM_WORLD, &mpi_size));                   \
                                                                                                        \
        { code }                                                                                        \
                                                                                                        \
        MPI_Finalize();                                                                                 \
        return 0;                                                                                       \
   }

#define _cmp(op, x, y)                                                                                  \
    ({                                                                                                  \
        typeof(x) __x = (x);                                                                            \
        typeof(y) __y = (y);                                                                            \
        __x op __y ? __x : __y;                                                                         \
    })
#define min(x, y) _cmp(<, x, y)
#define max(x, y) _cmp(>, x, y)

#endif // UTILITY_H__
